<?php

namespace Drupal\view_nid_field_sort\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("nid_field_sort_filter")
 */
class NidFieldSortFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $is_required = !$this->isExposed() || $this->options['expose']['required'];

    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#size' => 30,
      '#default_value' => $this->value,
      '#required' => $is_required,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {

    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }

    return $this->operator . ' ' . $this->value;
  }

  /**
   * {{ @inheritdoc }}.
   */
  public function query() {
  }

  /**
   * {{ @inheritdoc }}.
   */
  public function acceptExposedInput($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }
  }

  /**
   * {{ @inheritdoc }}.
   */
  protected function valueSubmit($form, FormStateInterface $form_state) {
    parent::valueSubmit($form, $form_state);
  }

}
