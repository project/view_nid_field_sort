<?php

namespace Drupal\view_nid_field_sort\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\view_nid_field_sort\Plugin\views\filter\NidFieldSortFilter;

/**
 * @ViewsSort("nid_field_sort")
 */
class NidFieldSort extends SortPluginBase {

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $this->ensureMyTable();

    if (!empty($this->view->filter)) {
      foreach ($this->view->filter as $key => $value) {
        if ($value instanceof NidFieldSortFilter) {
          $nids = explode(',', $value->value);
          foreach ($nids as $key => $nid) {
            $nids[$key] = (Int) $nid;
          }
          $nids = implode(",", $nids);
        }
      }
    }

    $input = $this->view->getExposedInput();
    if (!empty($input[$this->field])) {
      $nids = explode(',', $input[$this->field]);
      foreach ($nids as $key => $nid) {
        $nids[$key] = (Int) $nid;
      }
      $nids = implode(",", $nids);
    }

    if (!empty($nids)) {
      $sql_snippet = "FIELD($this->realField, $nids) ";
      $this->query->AddOrderBy(NULL, $sql_snippet, $this->options['order'], 'nid_field_sort_' . time());
    }
  }
}
